import json
import logging
import random
import socket
import uuid
from socket import socket, AF_INET, SOCK_STREAM, SHUT_RDWR
from threading import Thread
from typing import List

from peer.commands import PeerCommandsEnum
from peer.node_server import PeerNodeServer, LISTEN_FLAG
from server.commands import CoordinatorCommandsEnum
from server.dto_model import PeerDTO, RawPasswordDTO

logging.basicConfig()
logging.root.setLevel(logging.INFO)

logger = logging.getLogger(__name__)


class P2PClient:
    coordinator_addr = ("localhost", 5555)
    peer_server_thread: Thread
    own_peer_server_addr = ("127.0.0.1", random.randint(1024, 9999))
    connected_peers = []

    def __init__(self):
        self.socket_instance = socket(AF_INET, SOCK_STREAM)

        self._connect_to_coordinator()
        self.run_own_server_peer()

    def run_own_server_peer(self):
        Thread(target=PeerNodeServer(
            addr=self.own_peer_server_addr
        ).boot_coordinator).start()

    def run_password_manager(self):
        while LISTEN_FLAG:
            command = input("Send your command")

            if command == CoordinatorCommandsEnum.CONNECT:
                host, port = self.own_peer_server_addr
                command = f"{command} {port}"

            self.socket_instance.sendall(command.encode())
            #
            # if command == CoordinatorCommandsEnum.ALL_PEERS:
            #     self._get_members()

            if command == PeerCommandsEnum.ADD_PASSWORD:
                password_obj = self._input_for_password()
                available_peers = self._get_members()
                self.send_password_to_nodes(available_peers, password_obj)
            if command == PeerCommandsEnum.GET_PASSWORD:
                self.get_password_from_nodes()

    def _input_for_password(self):
        domain = input("Input your domain: ")
        password = input("Input your password (enter empty field for strong random password): ")
        if not password:
            # TODO: make password more strong
            password = uuid.uuid4().hex
            logger.info(f"Your password: {password}")
        return RawPasswordDTO(domain=domain, password=password)

    def _connect_to_coordinator(self):
        self.socket_instance.connect(self.coordinator_addr)

    def send_message(self, sock_message: str):
        self._send_to_tcp(sock_message.encode())

    def _send_to_tcp(self, sock_message: bytes):
        self.socket_instance.sendall(sock_message)

    def _get_members(self) -> List[PeerDTO]:
        self.socket_instance.sendall(CoordinatorCommandsEnum.ALL_PEERS.encode())

        bytes_response_data = self.socket_instance.recv(4096)
        response_data = bytes_response_data.decode()
        response_data = json.loads(response_data)
        if response_data:
            host, port = self.own_peer_server_addr
            own_addr_in_peers = [str(host), port]

            for peer in response_data:
                if peer['addr'] == own_addr_in_peers:
                    response_data.remove(peer)

            return [PeerDTO(**node_data) for node_data in response_data]

        logger.info("Peer list is empty. Try later.")
        return []

    def _connect_to_all(self, peers_for_connect: dict):
        pass

    def send_password_to_nodes(self, available_peers: List[PeerDTO], password_obj: RawPasswordDTO):
        for node_obj in available_peers:

            socket_for_sending = socket(AF_INET, SOCK_STREAM)
            socket_for_sending.connect(node_obj.addr)

            command = f"{PeerCommandsEnum.ADD_PASSWORD}|{password_obj.domain}|{password_obj.password}"

            socket_for_sending.sendall(command.encode())
            socket_for_sending.shutdown(SHUT_RDWR)  # Говорим ноде о приостановке коннекшена
            socket_for_sending.close()

    def get_password_from_nodes(self):
        pass

    def recv_message(self):
        data = self.socket_instance.recv(1024)


if __name__ == '__main__':
    P2PClient().run_password_manager()
