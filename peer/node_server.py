from server.coordinator import P2PNodesCoordinator, LISTEN_FLAG
from peer.commands import PeerCommandsEnum
import logging

logging.basicConfig()
logging.root.setLevel(logging.INFO)

logger = logging.getLogger(__name__)


class PeerNodeServer(P2PNodesCoordinator):
    def coordinate_nodes(self, connection, addr, message: str):
        super(PeerNodeServer, self).coordinate_nodes(connection, addr, message)

        if message.startswith(PeerCommandsEnum.ADD_PASSWORD):
            _, domain, password = message.split("|")
            print(f"Got domain: {domain} password: {password}. From addr: {addr}")
