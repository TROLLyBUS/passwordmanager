class CoordinatorCommandsEnum:
    CONNECT = "/connect"
    EXIT = "/exit"
    ALL_PEERS = "/members"

    RECEIVE_PASSWORD = "/add_password"
