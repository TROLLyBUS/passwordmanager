from typing import Tuple
from dataclasses import dataclass


@dataclass
class PeerDTO:
    addr: Tuple[str, int]
    uid: str

    def __init__(self, addr, uid):
        self.addr = tuple(addr)
        self.uid = uid

    def __str__(self):
        return f"{self.addr} {self.uid}"


@dataclass
class RawPasswordDTO:
    # Password for object to nodes
    domain: str
    password: str


@dataclass
class EncryptedPasswordDTO:
    # Password for object from nodes
    pass

