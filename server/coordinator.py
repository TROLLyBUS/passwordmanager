import json
import logging
import socket
import uuid
from logging import getLogger
from socket import socket, AF_INET, SOCK_STREAM, SOL_SOCKET, SO_KEEPALIVE
from threading import Thread
from typing import List
from typing import Tuple

from server.commands import CoordinatorCommandsEnum
from server.dto_model import PeerDTO
from util.json_encoder import EnhancedJSONEncoder

logging.basicConfig()
logging.root.setLevel(logging.INFO)

logger = getLogger(__name__)

LISTEN_FLAG = 1


class P2PNodesCoordinator:
    active_peers: List[PeerDTO] = []

    def __init__(self, socket_instance: socket = None, addr: Tuple[str, int] = ("localhost", 5555)):
        if not socket_instance:
            self.addr = addr
            self._s = socket(AF_INET, SOCK_STREAM)
            self._s.bind(self.addr)
        else:
            self._s = socket_instance

        self._s.setsockopt(SOL_SOCKET, SO_KEEPALIVE, 1000000)

    def boot_coordinator(self):
        print(self._s)
        self._s.listen()
        logger.info("Coordinator started.")
        while LISTEN_FLAG:
            connection, addr = self._s.accept()
            Thread(target=self.multiply_clients, args=(addr, connection), daemon=True).start()

    def multiply_clients(self, addr, connection):
        while 1:
            data = connection.recv(1024)
            if not data:
                logger.info(f"Connection {addr} wrote all data")
                break
            self.coordinate_nodes(connection, addr, data.decode())
        connection.close()

    def _add_new_peer(self, peer_addr, connection_string: str):
        clear_peer_port = connection_string.replace(CoordinatorCommandsEnum.CONNECT, "").replace(" ", "")
        peer_addr = list(peer_addr)
        peer_addr[1] = int(clear_peer_port)
        logger.info(f"Peer with addr: {peer_addr} connected.")
        new_peer_uid = str(uuid.uuid4())
        if peer_addr not in (peer.addr for peer in self.active_peers):
            self.active_peers.append(PeerDTO(peer_addr, new_peer_uid))

    def coordinate_nodes(self, connection, addr, message: str):
        if message.startswith(CoordinatorCommandsEnum.CONNECT):
            self._add_new_peer(addr, message)
        elif message == CoordinatorCommandsEnum.ALL_PEERS:
            self._get_members(connection)
        elif message == CoordinatorCommandsEnum.EXIT:
            pass
        else:
            logger.info(f"Received from {addr}: {message}")

    def _get_members(self, connection):
        logger.info(f"Active peers: {self.active_peers}")
        connection.sendall(json.dumps(self.active_peers, cls=EnhancedJSONEncoder).encode())


if __name__ == '__main__':
    P2PNodesCoordinator(addr=("localhost", 5555)).boot_coordinator()

    # TODO: Дописать add_password на уровне peer node server (хранение (dict like))
    # Написать get_password (как раз доставать из хранилища)
    # cyphers
